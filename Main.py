# Imports GUI moduals
import tkinter as TK
from Library.Utility import tkinter_basics


class Calculator():
    '''
    Stores the main calculator class
    '''

    def __init__(self, root, Username=None):
        '''
        Calls the constructors and stars loops
        '''
        # Assines root to the object
        self.root = root

        # Give the root key varables
        self.root.title("Calculator")
        self.root.wm_iconbitmap("Icon.ico")
        self.root.geometry("475x375")

        # Adds a cursor position varable so that it knows where to type
        self.Cursor_pos = 0

        self.list_pos = 0

        # The display string shows whats on the screen to the user
        self.Display_str = []
        # The sum string shows what python will achaly do
        self.Sum_str = []

        # The answer text
        self.Answer = ""

        # Holds last input for del button
        self.input_display_len = 0
        self.input_sum_len = 0

        # List of previous questions
        self.Previous_questions = []

        # Initsates the tkinter simplifier
        self.TB = tkinter_basics.Basics(Username=Username)

        # Creats the main frame to sit over the root window
        self.Main_fr = TK.Frame(self.root)
        self.Main_fr.grid(row=0, column=0, sticky="nsew")

        self.Calculator_fr = self.TB.AddFrame(self.Main_fr, Row=0, Column=0)
        self.Calculator_fr.grid(row=0, column=0, sticky="nsew", padx=0, pady=0)

        # Algins the grids so that they will fill the window root
        self.TB.Align_Grid(self.root)
        self.TB.Align_Grid(self.Main_fr)

        self.SetCalculator()

        return

    def SetCalculator(self):
        '''
        Creates the main display to show the colculator in its most basic form
        '''

        # Segments the display into frame components
        # For the output display
        self.Display_fr = self.TB.AddFrame(self.Calculator_fr, Row=0, Column=1)
        # Colour comments for debug perpouses
        # self.Display_fr.config(bg="pink")

        # For the exstened input options
        self.Exstended_fr = self.TB.AddFrame(
            self.Calculator_fr, Row=0, Column=0, RSpan=3)
        # self.Exstended_fr.config(bg="red")

        # For the users classis inputs
        self.Input_fr = self.TB.AddFrame(self.Calculator_fr, Row=1, Column=1)
        # self.Input_fr.config(bg="blue")

        # For other options and settings
        self.Options_fr = self.TB.AddFrame(self.Calculator_fr, Row=2, Column=1)
        # self.Options_fr.config(bg="green")

        # The sub frames for the componetns

        # For the arrow key inputes
        self.Arrow_fr = self.TB.AddFrame(
            self.Input_fr, Row=0, Column=0, CSpan=2)
        # self.Arrow_fr.config(bg="darkgreen")

        # For the number inputs
        self.Num_fr = self.TB.AddFrame(self.Input_fr, Row=1, Column=0)
        # self.Num_fr.config(bg="yellow")

        # For the symbol inputs
        self.Symbol_fr = self.TB.AddFrame(self.Input_fr, Row=1, Column=1)
        # self.Symbol_fr.config(bg="orange")

        # Sets the display_fr children
        self.SetDisplay()
        # Adds arrow keys to the display
        self.SetArrows()
        # Adds numbers to the display
        self.SetNumbers()
        # Adds the symbols to the screen
        self.SetSymbols()
        # Adds the exstened button to the frame
        self.SetExtension()
        # Adds the GUI to the options bar
        self.SetOptions()

        # Aligns all of the newly made frames
        self.TB.Align_Grid(self.Display_fr)
        self.TB.Align_Grid(self.Exstended_fr)
        self.TB.Align_Grid(self.Options_fr)
        self.TB.Align_Grid(self.Input_fr)
        self.TB.Align_Grid(self.Num_fr)
        self.TB.Align_Grid(self.Symbol_fr)
        self.TB.Align_Grid(self.Arrow_fr)
        # Algins the calculaor frame
        self.TB.Align_Grid(self.Calculator_fr)
        # Aligns the main_fr
        self.TB.Align_Grid(self.Main_fr)
        # Changes alignment of fist colum & row as this should be smaller then rest of display
        self.Calculator_fr.columnconfigure(0, weight=1)
        self.Calculator_fr.rowconfigure(0, weight=40)
        # self.Calculator_fr.rowconfigure(0, weight=5)
        # The sum will probally be longer then the answer so aliging accordingly
        self.Display_fr.columnconfigure(1, weight=8)
        return

    def SetDisplay(self):
        '''
        Puts all of the lables onto the display frame
        '''

        # The sum display lable
        self.SumDisplay_lbl = self.TB.AddLabel(
            self.Display_fr, "", Row=0, Column=0)
        self.SumDisplay_lbl.config(bg="white")

        # The answer display lable
        self.AnsDisplay_lbl = self.TB.AddLabel(
            self.Display_fr, "", Row=0, Column=1)
        self.AnsDisplay_lbl.config(bg="white")

        return

    def SetArrows(self):
        '''
        Adds the arrows on to the GUI for display navigation
        '''
        self.Arrow_list = ["↑", "↓", "←", "→", "Copy Ans"]

        for arrow in self.Arrow_list:
            Temp_Arrow = self.TB.AddButton(self.Arrow_fr, arrow, Row=0,
                                           Column=self.Arrow_list.index(arrow))
            Temp_Arrow.config(
                command=lambda Type=arrow: self.Arrow_command(Type))

        # SPAM AND EGGS - Add bindings to arrows

        return

    def SetNumbers(self):
        '''
        Puts all the items that appear in the number panel onto the GUI
        '''
        # Lists the value to appear in the display
        self.Nums = [
            ["7", "8", "9"],
            ["4", "5", "6"],
            ["1", "2", "3"],
            [".", "0", "Ans"]
        ]
        # Creats an array to store the buttons
        self.Nums_btns = [
            [] for _i in self.Nums
        ]

        for row in self.Nums:
            for column in row:
                Button = self.TB.AddButton(
                    self.Num_fr, column, Row=self.Nums.index(row), Column=row.index(column))
                self.Nums_btns[self.Nums.index(row)].append(Button)
                Button.config(command=lambda Num=column: self.AddNum(Num))
        # SPAM AND EGGS - Add bindings to keys
        return

    def SetSymbols(self):
        '''
        Puts all of the sybols such as +-*/ etc. on to the GUI
        '''
        # Lists the values to appear in the display
        self.Symb = [
            ["Del", "C"],
            ["+", "-"],
            ["*", "/"],
            ["^", "n√("],
            ["(", ")"],
        ]
        self.Special_symb = ["Del", "C", "^", "n√("]

        # Creats an empty list to sotre the buttons
        self.Symb_btns = [
            [] for _i in self.Symb
        ]

        for row in self.Symb:
            for column in row:
                Symb = self.TB.AddButton(
                    self.Symbol_fr, column, Row=self.Symb.index(row), Column=row.index(column))
                self.Symb_btns[self.Symb.index(row)].append(Symb)
                Symb.config(command=lambda Symb=column: self.AddSymb(Symb))
        # SPAM AND EGGS - Add binding to keys
        return

    def SetExtension(self):
        '''
        Adds the button which hides/revealses the other options
        '''
        self.Exstended = False
        self.Exstend_btn = self.TB.AddButton(
            self.Exstended_fr, "<", Row=0, Column=1)
        self.Exstend_btn.config(
            command=lambda: self.ShowExstension())

        # SPAM AND EGGS- Add commands to exstension

        return

    def ShowExstension(self):
        '''
        Hides or revals the exstened options e.g. sin cos tan etc.
        '''
        # Switches the boolean of the exstanded var
        self.Exstended = not self.Exstended

        # If it should become exstened
        if self.Exstended:
            # Change the buttons text for chlarity
            self.Exstend_btn.config(text=">")
            # Add a new frame to hold the buttons
            self.ExtraSymb_fr = self.TB.AddFrame(
                self.Exstended_fr, Row=0, Column=0)

            # List storing the symbols values
            self.ExtraSymbs = [
                ["sin(", "cos(", "tan("],
                ["sin-1(", "cos-1(", "tan-1("],
                ["π", "*10^"]
            ]
            # List to store the buttons so that they can be used later
            self.ExtraSymbs_btns = [
                [] for _i in self.ExtraSymbs
            ]

            # Checks if these buttons have been added to the special char list for the symb inputs
            if self.ExtraSymbs[0][0] not in self.Special_symb:
                # Adds every needed symble to the extra symble frame
                for Symb_list in self.ExtraSymbs:
                    self.Special_symb.extend(Symb_list)

            # Adds in all of the buttons for the exstended symbels
            for row in self.ExtraSymbs:
                for column in row:
                    # Adds in the buttons stored in a temp var
                    Temp_symb = self.TB.AddButton(
                        self.ExtraSymb_fr, column, Row=self.ExtraSymbs.index(row), Column=row.index(column))
                    # Adds the buttons the the perminent list
                    self.ExtraSymbs_btns.append(Temp_symb)
                    # Adds the command to the buttons
                    Temp_symb.config(
                        command=lambda Symb=column: self.AddSymb(Symb))

            # Fixes GUI expansion tidiness
            self.Exstended_fr.columnconfigure(0, weight=5)

            # Changes the root windows size
            self.root.geometry("600x375")

            self.TB.Align_Grid(self.ExtraSymb_fr)

        else:
            # Replaces the text with defults
            self.Exstend_btn.config(text="<")
            # Destroys the old frame
            self.ExtraSymb_fr.destroy()

            # Re-sizes the root
            self.root.geometry("475x375")

        return

    def SetOptions(self):
        '''
        Adds buttons to the GUI for options e.g. settings, quit and equals
        '''

        # Adds a quit button
        self.Quit_btn = self.TB.AddButton_negetive(
            self.Options_fr, "Quit", Row=0, Column=0)
        self.Quit_btn.config(command=lambda: self.Quit())

        # Adds the settings button
        self.Setting_btn = self.TB.AddButton(
            self.Options_fr, "Settings", Row=0, Column=1)
        self.Setting_btn.config(command=lambda: self.Settings())

        # Adds the equals buttons
        self.Equals_btn = self.TB.AddButton(
            self.Options_fr, "=", Row=0, Column=2
        )
        self.Equals_btn.config(command=lambda: self.RunSum())

        return

    def CursorToggle(self, Visable):
        '''
        Adds or removes a cursor indicator so you know where you will type
        '''
        # print(self.Display_str.count("|"))
        # print("Toggle")

        if Visable:
            self.Display_str.insert(self.Cursor_pos, "|")
            return
        try:
            self.Display_str.remove("|")
        except ValueError:
            # if the user tries to move the curser on a normal delete phase this will call
            pass

    def Arrow_command(self, Type):
        '''
        Updates the position based off the arrow keys
        '''

        # Tests which arrow has been pressed
        if Type == "↑":
            self.SumUp()
        if Type == "↓":
            self.SumDown()
        if Type == "←":
            # If the cursor is at the start don't move it
            if self.Cursor_pos == 0:
                self.Cursor_pos = 0
                self.CursorToggle(False)
                self.CursorToggle(True)
            else:
                # Otherwise move the cursor back one position
                self.Cursor_pos -= 1
                self.CursorToggle(False)
                self.CursorToggle(True)
        if Type == "→":
            # If it is at the end leave it there
            if self.Cursor_pos >= len(self.Display_str)-1:
                #self.Cursor_pos = len(self.Display_str) - 1
                pass
            else:
                # Or increase its position by one
                self.Cursor_pos += 1
                self.CursorToggle(False)
                self.CursorToggle(True)
        if Type == "Copy Ans":
            # Clear the clip board
            self.root.clipboard_clear()
            # add text to clipboard
            self.root.clipboard_append(self.Answer)

        return

    def SumUp(self):
        '''
        Loads the previous sum
        '''
        # Resets the cursors position
        self.Cursor_pos = 0
        # Moves it back in the list
        self.list_pos -= 1

        try:
            # Tries to display the new text
            self.Display_str = list(self.Previous_questions[self.list_pos])
        except IndexError:
            # If it goes past the end of the list
            # Add one so that this will be triggared again and do nothing
            self.list_pos += 1
            pass

        # Updates the label's text
        self.SumDisplay_lbl.config(text="".join(self.Display_str))

        return

    def SumDown(self):
        '''
        Displays more resent sums
        '''
        # Resets the cursor positon
        self.Cursor_pos = 0
        # If its not the last item on the list
        if not self.list_pos >= 0:
            # Move the list further down
            self.list_pos += 1
        else:
            # If it is the last item do nothing
            return

        # If it is the 0th item it must be a new request
        if self.list_pos == 0:
            self.Display_str = []
        else:

            try:
                # Puts the sum into the string in list form
                self.Display_str = list(self.Previous_questions[self.list_pos])
            except IndexError:
                pass

        # Updates the display text
        self.SumDisplay_lbl.config(text="".join(self.Display_str))

        return

    def Settings(self):
        '''
        Loads the options menu GUI
        '''

        self.TB.WorkInProgress()

        return

    def AddNum(self, Num):
        '''
        Adds a given number to the display and sum strings
        '''

        # Test if the answer key has been pressed
        if Num.lower() == "ans" and self.Answer:
            # Lists the previous answer
            Num = list(self.Answer)
            # For every character in the privious number add it to the display and sum
            for char in Num:
                self.Display_str.insert(self.Cursor_pos, char)
                self.Sum_str.insert(self.Cursor_pos, char)
                self.Cursor_pos += 1

        # If its not anser it must be a standered character
        elif Num.lower() != "ans":

            # Insert the number into the diplay and sum strings
            self.Display_str.insert(self.Cursor_pos, Num)
            self.Sum_str.insert(self.Cursor_pos, Num)

            # Update the display
            self.SumDisplay_lbl.config(text="".join(self.Display_str))

            self.Cursor_pos += 1
        else:
            # Gets call if the user presses ans when there isn't an answer to use
            pass

        return

    def AddSymb(self, Symb):
        '''
        Adds a symbol to the display and sum strings
        '''

        # if the symbol is non-standered it will handel it individuall
        if Symb in self.Special_symb:
            # For simplication
            Symb = Symb.lower()
            # Delete the preivous input from sum and display
            if Symb == "del":
                for _i in range(self.input_display_len):
                    self.Display_str.pop(self.Cursor_pos-1)
                    self.Cursor_pos -= 1
                self.SumDisplay_lbl.config(text="".join(self.Display_str))

                pass

            # Completely wipes the sum and siplay
            elif Symb == "c":
                pass

            # x to the power of y
            elif Symb == "^":
                pass
            # For root commands
            elif Symb == "n√(":
                pass
            # sin
            elif Symb == "sin(":
                pass
            # co sin
            elif Symb == "cos(":
                pass
            # tan
            elif Symb == "tan(":
                pass
            # sin to minus one
            elif Symb == "sin-1(":
                pass
            # cos to minus one
            elif Symb == "cos-1(":
                pass
            # tan  to minus one
            elif Symb == "tan-1(":
                pass
            # pi
            elif Symb == "π":
                pass
            # Times ten to the power of x
            elif Symb == "*10^(":
                pass
            else:
                print(Symb)
        # If its a normal symbol run this
        else:
            # adds the symbol to the display and sum strings
            self.Display_str.insert(self.Cursor_pos, Symb)
            self.Sum_str.insert(self.Cursor_pos, Symb)

            # Updates the display
            self.SumDisplay_lbl.config(text="".join(self.Display_str))
            # Updates the cursor
            self.CursorToggle(False)
            self.CursorToggle(True)

            self.Cursor_pos += 1

        # Gives the length of the input- this helps with deleting it
        self.input_display_len = len(Symb)
        self.input_sum_len = len(Symb)

        return

    def RunSum(self):
        '''
        Runs the given sum and diplays the output
        '''
        print("".join(self.Sum_str))

        return

    def Quit(self):
        '''
        Exits the program and destroyes the root
        '''
        self.root.destroy()


def Main_loop(root, Username):
    '''
    Calls the main loop and handels any exception
    '''
    # Gets instance of main class
    Main = Calculator(root, Username=Username)

    # Sets the loop number to 0
    Loop = 0
    # Sets the cursor visability to True
    Visable = True
    while True:
        # Fixes and odd bug where the | gets duplicated
        # I tried to find why but couldn't so have a dougy workaround -_-
        if Main.Display_str.count("|") > 1:
            Main.Display_str.remove("|")

        # print(Loop)
        # If the loop is 5000
        if Loop == 1000:
            # Toggle the cursor to be visable or not visable
            Main.CursorToggle(Visable)
            # print(Main.Display_str)
            # Change what visable will be
            Visable = not Visable
            # Reset the loop
            Loop = 0
        # Add one to the loop
        Loop += 1

        # Updates the displays text for the cursor
        Main.SumDisplay_lbl.config(text="".join(Main.Display_str))

        try:
            # Updates the root
            root.update()
            root.update_idletasks()
        # If the root has been closed- exit
        except:
            return


def Run(root=None, Username=None):
    '''
    External programs can call this to run the program
    '''
    # If there isn't a root it creats one
    if not root:
        root = TK.Tk()

    # Calls the main program loop
    Main_loop(root, Username)

    return


if __name__ == "__main__":
    '''
    Will run the caluclator
    '''
    # Creats a root object for thr GUI
    root = TK.Tk()

    # Calls the main program loop
    Main_loop(root, None)

    # When the loop exits- quit the program safley
    raise SystemExit
